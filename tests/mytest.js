import test from 'ava';
import supertest from 'supertest';
const app = require('../app');
const request = supertest(app);


test.serial('get users', async t => {
  const response= await request
    .get('/users')
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response.statusCode, 200)
    const users=response.body;
    for (var i=0; i<users.length; i++)
        {
            const response2= await request
            .get('/users/'+users[i]._id)
            .set('content-type', 'application/json')
            .expect(200);
            t.is(response2.statusCode, 200)
        }
    for (var i=0; i<users.length; i++)
        {
            const response3= await request
            .get('/users/'+users[i]._id+'/categories/')
            .set('content-type', 'application/json')
            .expect(200);
            t.is(response3.statusCode, 200)
        }
});

test.serial('create user', async t  => {
    const response = await request
    .post('/users')
    .set('content-type', 'application/json')
    .send({firstname: 'vasya', lastname: 'pupkin', email: 'pupkin.v@gmail.com'})
    .expect(200);
    t.is(response.statusCode, 200)
    // Create copy of user with email pupkin.v@gmail.com must be refused 
    const response2 = await request
    .post('/users')
    .set('content-type', 'application/json')
    .send({firstname: 'vasya', lastname: 'pupkin', email: 'pupkin.v@gmail.com'})
    .expect(404);
    t.is(response2.statusCode, 404)
});


test.serial('patch user', async t => {
     const response= await request
    .get('/users')
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response.statusCode, 200)
    const users=response.body;
    const user_patch = users.find((element) => element.email === 'pupkin.v@gmail.com');
    const response2 = await request
    .patch('/users/'+user_patch._id)
    .set('content-type', 'application/json')
    .send({ firstname: 'VAsya',lastname: 'Pupkin', email: 'pupkin.v@gmail.com'})
    .expect(200);
    t.is(response2.statusCode, 200)
});


test.serial('put user', async t => {
     const response= await request
    .get('/users')
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response.statusCode, 200)
    const users=response.body;
    const user_put = users.find((element) => element.email === 'pupkin.v@gmail.com');
    const response2 = await request
    .put('/users/'+user_put._id)
    .set('content-type', 'application/json')
    .send({ firstname: 'VASYA', lastname: 'PUP King', email: 'pupkin.v@gmail.com'})
    .expect(200);
    t.is(response2.statusCode, 200)
});


test.serial('get categories', async t => {
  const response= await request
    .get('/categories')
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response.statusCode, 200)
    const categories=response.body;
    for (var i=0; i<categories.length; i++)
        {
            const response2= await request
            .get('/categories/'+categories[i]._id)
            .set('content-type', 'application/json')
            .expect(200);
            t.is(response2.statusCode, 200)
        }
        for (var i=0; i<categories.length; i++)
        {
            const response3= await request
            .get('/categories/'+categories[i]._id+'/products/')
            .set('content-type', 'application/json')
            .expect(200);
            const products=response3.body;
            //var prods = products.reduce((all, current) => {return all+current.name+' '},'{');
            //prods+='}';
            // console.log ('categories -'+categories[i].name+ ' include products '+response3.body.length+' = '+prods);
            t.is(response3.statusCode, 200)
        }
});

test.serial('create category', async t => {
  const response= await request
    .get('/users')
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response.statusCode, 200)
    const users=response.body;
    const user_author = users.find((element) => element.email === 'pupkin.v@gmail.com');
    const response2= await request
    .post('/categories')
    .set('content-type', 'application/json')
    .send({ name: 'Vasina category',user: user_author._id})
    .expect(200);
    t.is(response2.statusCode, 200)
        const categories=response2.body;
        for (var i=0; i<categories.length; i++)
        {
            const response3= await request
            .get('/categories/'+categories[i]._id+'/products/')
            .set('content-type', 'application/json')
            .send({ name: 'Vasina category',user:user_author._id })
            .expect(200);
            const products=response3.body;
           // var prods = products.reduce((all, current) => {return all+current.name+' '},'{');
           // prods+='}';
           // console.log ('categories -'+categories[i].name+ ' include products '+response3.body.length+' = '+prods);
            t.is(response3.statusCode, 200)
        }
});

test.serial('patch category', async t => {
  const response= await request
    .get('/categories')
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response.statusCode, 200)
    const categories=response.body;
    const category_patch = categories.find((element) => element.name == 'Vasina category');
    const response2= await request
    .patch('/categories/'+category_patch._id)
    .send({name: 'Vasina favorite category'})
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response2.statusCode, 200)
});


test.serial('put category', async t => {
  const response= await request
    .get('/categories')
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response.statusCode, 200)
    const categories=response.body;
    const category_put = categories.find((element) => element.name == 'Vasina favorite category');
    const response2= await request
    .put('/categories/'+category_put._id)
    .send({name: 'Vasina best favorite category', id: category_put._id})
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response2.statusCode, 200)
});

test.serial('get products', async t => {
  const response= await request
    .get('/products')
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response.statusCode, 200)
    const products=response.body;
    for (var i=0; i<products.length; i++)
    { 
     const response2= await request
     .get('/products/'+products[i]._id)
     .set('content-type', 'application/json')
     .expect(200);
     t.is(response2.statusCode, 200)
    }
});

test.serial('create product', async t => {
  const response= await request
    .get('/categories')
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response.statusCode, 200)
    const categories=response.body;
    const category = categories.find((element) => element.name == 'Vasina best favorite category');
    const response2= await request
    .post('/products')
    .send({name: 'Vasin product', category: category._id})
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response2.statusCode, 200)
});


test.serial('patch product', async t => {
  const response= await request
    .get('/products')
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response.statusCode, 200)
    const products=response.body;
    const product_patch = products.find((element) => element.name == 'Vasin product');
    const response2= await request
    .patch('/products/'+product_patch._id)
    .send({name: 'Vasin favorite product'})
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response2.statusCode, 200)
});

test.serial('put product', async t => {
  const response= await request
    .get('/products')
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response.statusCode, 200)
    const products=response.body;
    const product_put = products.find((element) => element.name == 'Vasin favorite product');
    const response2= await request
    .patch('/products/'+product_put._id)
    .send({name: 'Vasin best favorite product', category: product_put.category})
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response2.statusCode, 200)
});



test.serial('delete product', async t => {
  const response= await request
    .get('/products')
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response.statusCode, 200)
    const products=response.body;
    const product_del = products.find((element) => element.name == 'Vasin best favorite product');
    const response2= await request
    .delete('/products/'+product_del._id)
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response2.statusCode, 200)
});




test.serial('delete categories', async t => {
  const response= await request
    .get('/categories')
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response.statusCode, 200)
    const categories=response.body;
    const category_del = categories.find((element) => element.name == 'Vasina best favorite category');
    const response2= await request
    .delete('/categories/'+category_del._id)
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response2.statusCode, 200)
});



test.serial('delete user', async t => {
     const response= await request
    .get('/users')
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response.statusCode, 200)
    const users=response.body;
    for (var i=0; i<users.length; i++)
        {
          if (users[i].email=='pupkin.v@gmail.com') {
          const response2 = await request
          .delete('/users/'+users[i]._id)
          .set('content-type', 'application/json')
          .expect(200); 
          t.is(response2.statusCode, 200)    
          break; 
          }
        }
});
